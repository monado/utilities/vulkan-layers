# Copyright 2022 Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

<#
.SYNOPSIS
Configure a Vulkan implicit API layer
.DESCRIPTION
Use the parameters to either install or uninstall a Vulkan implicit API layer
.PARAMETER Manifest
Path to the JSON manifest for the layer
.PARAMETER User
Install/uninstall in the current user registry hive (HKCU), rather than the system hive (HKLM).
.PARAMETER Uninstall
Remove the entries added during the installation instead of adding them
#>
[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]
    $Manifest,
    [Parameter()]
    [switch]
    $User = $false,
    [Parameter()]
    [switch]
    $Uninstall = $false
)

$ErrorActionPreference = "Stop"
$Manifest = (Get-Item $Manifest -ErrorAction Stop).FullName

$Hive = "HKLM:"
if ($User) {
    $Hive = "HKCU:"
}
$RegPath = "$Hive\SOFTWARE\Khronos\Vulkan\ImplicitLayers"

if ($Uninstall) {
    Write-Host "Unregistering manifest $Manifest from $RegPath"
    Remove-ItemProperty -Path $RegPath -Name "$Manifest" -ErrorAction SilentlyContinue
}
else {
    Write-Host "Registering manifest $Manifest in $RegPath"
    if (!(Test-Path $RegPath)) {
        New-Item -Path $RegPath -Force
    }
    New-ItemProperty -Path $RegPath -Name "$Manifest" -Value 0 -PropertyType "DWord" | Out-Null
}

# 
