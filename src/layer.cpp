// Copyright 2022, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "vulkan/vulkan_core.h"
#include "vulkan/vk_layer.h"

#include "env_option.h"

#include <vector>
#include <string>

#include <stdio.h>
#include <string.h>

#ifndef VK_LAYER_EXPORT
#if defined(__GNUC__) && __GNUC__ >= 4
#define VK_LAYER_EXPORT __attribute__((visibility("default")))
#elif defined(__SUNPRO_C) && (__SUNPRO_C >= 0x590)
#define VK_LAYER_EXPORT __attribute__((visibility("default")))
#else
#define VK_LAYER_EXPORT
#endif
#endif

/*
 *
 * Helpers
 *
 */

DEBUG_GET_ONCE_BOOL_OPTION(print, "ENABLE_TIMELINE_SEMAPHORE_LOG", false)

#define LOG(str)                                                                                                       \
	do {                                                                                                           \
		if (debug_get_bool_option_print()) {                                                                   \
			fprintf(stderr, "%s: %s\n", __func__, str);                                                    \
		}                                                                                                      \
	} while (false)

#if 0
#define ENTER_FUNCTION_PRINT() LOG(str)
#else
#define ENTER_FUNCTION_PRINT()
#endif

template <typename T, VkStructureType type>
static T *
getLayerCreateInfo(const void *pNext)
{
	T *layerCreateInfo = (T *)pNext;

	// step through the chain of pNext until we get to the link info
	while (layerCreateInfo && (layerCreateInfo->sType != type || layerCreateInfo->function != VK_LAYER_LINK_INFO)) {
		layerCreateInfo = (T *)layerCreateInfo->pNext;
	}

	return layerCreateInfo;
}

static VkLayerInstanceCreateInfo *
getInstanceLayerCreateInfo(const void *pNext)
{
	return getLayerCreateInfo<VkLayerInstanceCreateInfo, VK_STRUCTURE_TYPE_LOADER_INSTANCE_CREATE_INFO>(pNext);
}

static VkLayerDeviceCreateInfo *
getDeviceLayerCreateInfo(const void *pNext)
{
	return getLayerCreateInfo<VkLayerDeviceCreateInfo, VK_STRUCTURE_TYPE_LOADER_DEVICE_CREATE_INFO>(pNext);
}

static bool
hasChainStruct(const void *pNext, VkStructureType sType)
{
	VkBaseInStructure const *base = (const VkBaseInStructure *)pNext;

	while (base != NULL) {
		if (base->sType == sType) {
			return true;
		}

		base = (const VkBaseInStructure *)base->pNext;
	}

	return false;
}


/*
 *
 * Layer lists.
 *
 */

#define ENTRY_COUNT 16

struct InstanceEntry
{
	VkInstance instance{};
	PFN_vkGetInstanceProcAddr getInstanceProcAddr{};
	PFN_vkDestroyInstance destroyInstance{};
};

struct DeviceEntry
{
	VkDevice device{};
	PFN_vkGetDeviceProcAddr getDeviceProcAddr{};
	PFN_vkDestroyDevice destroyDevice{};
};

static InstanceEntry gInstanceList[ENTRY_COUNT]{};
static DeviceEntry gDeviceList[ENTRY_COUNT]{};

static InstanceEntry *
getInstanceEntry(VkInstance instance)
{
	InstanceEntry *entry = NULL;
	for (int i = 0; i < ENTRY_COUNT; i++) {
		if (gInstanceList[i].instance != instance) {
			continue;
		}

		return &gInstanceList[i];
	}

	return NULL;
}

static void
destroyInstanceEntry(InstanceEntry *entry)
{
	entry->getInstanceProcAddr = NULL;
	entry->destroyInstance = NULL;
	entry->instance = VK_NULL_HANDLE;
}

static DeviceEntry *
getDeviceEntry(VkDevice device)
{
	DeviceEntry *entry = NULL;
	for (int i = 0; i < ENTRY_COUNT; i++) {
		if (gDeviceList[i].device != device) {
			continue;
		}

		return &gDeviceList[i];
	}

	return NULL;
}

static void
destroyDeviceEntry(DeviceEntry *entry)
{
	entry->device = VK_NULL_HANDLE;
	entry->destroyDevice = NULL;
	entry->getDeviceProcAddr = NULL;
}


/*
 *
 * Layer functions.
 *
 */

static VkResult VKAPI_CALL
EnableTimeline_CreateInstance(const VkInstanceCreateInfo *pCreateInfo,
                              const VkAllocationCallbacks *pAllocator,
                              VkInstance *pInstance)
{
	ENTER_FUNCTION_PRINT();

	VkLayerInstanceCreateInfo *layerCreateInfo = getInstanceLayerCreateInfo(pCreateInfo->pNext);
	if (layerCreateInfo == NULL) {
		// No loader instance create info
		return VK_ERROR_INITIALIZATION_FAILED;
	}

	PFN_vkGetInstanceProcAddr getInstanceProcAddr = layerCreateInfo->u.pLayerInfo->pfnNextGetInstanceProcAddr;

	// Move chain on for next layer
	layerCreateInfo->u.pLayerInfo = layerCreateInfo->u.pLayerInfo->pNext;

	PFN_vkCreateInstance createFunc = (PFN_vkCreateInstance)getInstanceProcAddr(VK_NULL_HANDLE, "vkCreateInstance");

	VkResult ret = createFunc(pCreateInfo, pAllocator, pInstance);
	if (ret != VK_SUCCESS) {
		// Failed to create instance.
		return ret;
	}

	// Find the first empty layer entry.
	InstanceEntry *entry = getInstanceEntry(VK_NULL_HANDLE);
	if (entry == NULL) {
		return VK_ERROR_INITIALIZATION_FAILED;
	}

	entry->instance = *pInstance;
	entry->getInstanceProcAddr = getInstanceProcAddr;
	entry->destroyInstance = (PFN_vkDestroyInstance)getInstanceProcAddr(*pInstance, "vkDestroyInstance");

	return ret;
}

static VKAPI_ATTR void VKAPI_CALL
EnableTimeline_DestroyInstance(VkInstance instance, const VkAllocationCallbacks *pAllocator)
{
	ENTER_FUNCTION_PRINT();

	InstanceEntry *entry = getInstanceEntry(instance);
	entry->destroyInstance(instance, pAllocator);

	destroyInstanceEntry(entry);
}

static VKAPI_ATTR VkResult VKAPI_CALL
EnableTimeline_CreateDevice(VkPhysicalDevice physicalDevice,
                            const VkDeviceCreateInfo *pCreateInfo,
                            const VkAllocationCallbacks *pAllocator,
                            VkDevice *pDevice)
{
	ENTER_FUNCTION_PRINT();

	VkLayerDeviceCreateInfo *layerCreateInfo = getDeviceLayerCreateInfo(pCreateInfo->pNext);
	if (layerCreateInfo == NULL) {
		// No loader device create info.
		return VK_ERROR_INITIALIZATION_FAILED;
	}

	PFN_vkGetInstanceProcAddr getInstanceProcAddr = layerCreateInfo->u.pLayerInfo->pfnNextGetInstanceProcAddr;
	PFN_vkGetDeviceProcAddr getDeviceProcAddr = layerCreateInfo->u.pLayerInfo->pfnNextGetDeviceProcAddr;

	// Move chain on for next layer.
	layerCreateInfo->u.pLayerInfo = layerCreateInfo->u.pLayerInfo->pNext;

	PFN_vkCreateDevice createFunc = (PFN_vkCreateDevice)getInstanceProcAddr(VK_NULL_HANDLE, "vkCreateDevice");


	/*
	 * Add extension and timeline semaphore enablement struct.
	 */

	VkDeviceCreateInfo createInfo = *pCreateInfo;

	std::vector<const char *> array{};

	bool found = false;
	for (uint32_t i = 0; i < createInfo.enabledExtensionCount; i++) {
		const char *str = createInfo.ppEnabledExtensionNames[i];

		if (strcmp(str, VK_KHR_TIMELINE_SEMAPHORE_EXTENSION_NAME) == 0) {
			found = true;
		}

		// Push all of the extensions to the array as well.
		array.push_back(str);
	}

	if (!found) {
		array.push_back(VK_KHR_TIMELINE_SEMAPHORE_EXTENSION_NAME);

		// Update array with new extension.
		createInfo.ppEnabledExtensionNames = &array[0];
		createInfo.enabledExtensionCount++;
	}

	VkPhysicalDeviceTimelineSemaphoreFeatures timelineSemaphore{};
	timelineSemaphore.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES;
	timelineSemaphore.timelineSemaphore = true;
	timelineSemaphore.pNext = (void *)createInfo.pNext; // Have to cast here.

	if (hasChainStruct(createInfo.pNext, VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES)) {
		LOG("Found VkPhysicalDeviceTimelineSemaphoreFeatures in next chain, not adding.");
	} else {
		LOG("Adding VkPhysicalDeviceTimelineSemaphoreFeatures to next chain.");
		createInfo.pNext = &timelineSemaphore;
	}

	VkResult ret = createFunc(physicalDevice, &createInfo, pAllocator, pDevice);
	if (ret != VK_SUCCESS) {
		// Failed to create device.
		return ret;
	}

	// Find the first empty layer entry.
	DeviceEntry *entry = getDeviceEntry(VK_NULL_HANDLE);
	if (entry == NULL) {
		return VK_ERROR_INITIALIZATION_FAILED;
	}

	entry->device = *pDevice;
	entry->getDeviceProcAddr = getDeviceProcAddr;
	entry->destroyDevice = (PFN_vkDestroyDevice)getDeviceProcAddr(*pDevice, "vkDestroyDevice");

	return VK_SUCCESS;
}

static VKAPI_ATTR void VKAPI_CALL
EnableTimeline_DestroyDevice(VkDevice device, const VkAllocationCallbacks *pAllocator)
{
	ENTER_FUNCTION_PRINT();

	DeviceEntry *entry = getDeviceEntry(device);
	entry->destroyDevice(device, pAllocator);

	destroyDeviceEntry(entry);
}

static VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL
EnableTimeline_GetInstanceProcAddr(VkInstance instance, const char *pName)
{
	if (strcmp(pName, "vkCreateInstance") == 0) {
		return (PFN_vkVoidFunction)EnableTimeline_CreateInstance;
	}
	if (strcmp(pName, "vkDestroyInstance") == 0) {
		return (PFN_vkVoidFunction)EnableTimeline_DestroyInstance;
	}
	if (strcmp(pName, "vkCreateDevice") == 0) {
		return (PFN_vkVoidFunction)EnableTimeline_CreateDevice;
	}

	return getInstanceEntry(instance)->getInstanceProcAddr(instance, pName);
}

static VKAPI_ATTR PFN_vkVoidFunction VKAPI_CALL
EnableTimeline_GetDeviceProcAddr(VkDevice device, const char *pName)
{
	if (strcmp(pName, "vkDestroyDevice") == 0) {
		return (PFN_vkVoidFunction)EnableTimeline_DestroyDevice;
	}

	return getDeviceEntry(device)->getDeviceProcAddr(device, pName);
}

VK_LAYER_EXPORT VKAPI_ATTR VkResult VKAPI_CALL
vkNegotiateLoaderLayerInterfaceVersion(VkNegotiateLayerInterface *pVersionStruct)
{
	ENTER_FUNCTION_PRINT();

	if (pVersionStruct->loaderLayerInterfaceVersion < 2) {
		return VK_ERROR_INITIALIZATION_FAILED;
	}

	pVersionStruct->loaderLayerInterfaceVersion = 2;
	pVersionStruct->pfnGetInstanceProcAddr = EnableTimeline_GetInstanceProcAddr;
	pVersionStruct->pfnGetDeviceProcAddr = EnableTimeline_GetDeviceProcAddr;

	return VK_SUCCESS;
}
