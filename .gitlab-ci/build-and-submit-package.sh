#!/usr/bin/env bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2018-2022 Collabora, Ltd. and the Monado contributors
#
# Requires some environment variables (normally set by CI)
# Any extra args get passed to debuild, so try -B for a local binary-only build

CI_COMMIT_SHA=${CI_COMMIT_SHA:-HEAD}
CODENAME=${CODENAME:-focal}
DISTRO=${CODENAME:-ubuntu}
DEB_VERSION_SUFFIX=${DEB_VERSION_SUFFIX:-ci}
set -euo pipefail

echo "DISTRO ${DISTRO}"
echo "CODENAME ${CODENAME}"
echo "DEB_VERSION_SUFFIX ${DEB_VERSION_SUFFIX}"
echo "CI_COMMIT_SHA ${CI_COMMIT_SHA}"
echo "FULL_VER ${FULL_VER}"
echo "DATESTAMP ${DATESTAMP}"

# Prep the source tree: grab the debian directory from the packaging branch.
git remote update --prune
git checkout origin/debian -- debian/

PKGNAME=$(dpkg-parsechangelog --show-field source)

mv debian ${PKGNAME}/debian

INCOMING="$(pwd)/incoming"
export INCOMING
mkdir -p "$INCOMING"

cd monado-vulkan-layers

export DEVSCRIPTS_CHECK_DIRNAME_LEVEL=0

DECORATED="${FULL_VER}-1~${DATESTAMP}${DEB_VERSION_SUFFIX}"
# -b forces the version even though it's "worse" than the previous one.
dch --newversion "${DECORATED}" --preserve "Automated CI build of commit ${CI_COMMIT_SHA}" -b

# Stash the package version in a convenient file for a later job.
echo "${DECORATED}" > "${INCOMING}/${CODENAME}.distro"
echo "${CODENAME}_pkgversion=${DECORATED}" > "../${CODENAME}.env"

# Build the package
debuild -uc -us "$@"

cd ..

# Use dput-ng to move the package-related files into some artifacts.
mkdir -p ~/.dput.d/profiles
envsubst < .gitlab-ci/localhost.json > ~/.dput.d/profiles/localhost.json

eval "$(dpkg-architecture)"

dput --debug localhost "${PKGNAME}_${DECORATED}_${DEB_BUILD_ARCH}.changes"
