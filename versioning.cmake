# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2018-2022 Collabora, Ltd. and the Monado contributors

# This script is for use both within cmake configuration, as well as in script mode.
# Script mode usage is something like this:
#   cmake "-DINPUT=$(git describe --always)" -P versioning.cmake

function(monado_process_git_describe input outvar)
	# This is our "single source of truth" for converting git-describe output into versions.
	string(REGEX REPLACE "-([0-9]+)-g([0-9a-f]+)" ".\\1.git.\\2" result "${input}")
	set(${outvar}
	    "${result}"
	    PARENT_SCOPE
		)
endfunction()

if(CMAKE_SCRIPT_MODE_FILE)
	monado_process_git_describe(${INPUT} OUTPUT)
	message("${OUTPUT}")
endif()
